using System;
using System.Runtime.InteropServices;
using System.Threading;
using System.Windows.Forms;

namespace RemoteController
{
    public partial class DeviceHandler
    {
        private const int MOUSEEVENT_LEFTDOWN = 0x02;
        private const int MOUSEEVENT_LEFTUP = 0x04;
        private const int MOUSEEVENT_RIGHTDOWN = 0x08;
        private const int MOUSEEVENT_RIGHTUP = 0x10;

        [DllImport("user32.dll", CharSet = CharSet.Auto, CallingConvention = CallingConvention.StdCall)]
        public static extern void mouse_event(uint dwFlags, uint dx, uint dy, uint cButtons, uint dwExtraInfo);

        public static void MouseClick(int c)
        {
            uint x = (uint)Cursor.Position.X;
            uint y = (uint)Cursor.Position.Y;
            for (int i = 0; i < c; i++)
            {
                mouse_event(MOUSEEVENT_LEFTDOWN, x, y, 0, 0);
                mouse_event(MOUSEEVENT_LEFTUP, x, y, 0, 0);
                Thread.Sleep(50);
            }
        }
        public static void MouseMove(int x, int y)
        {
            Cursor.Position = new System.Drawing.Point(x, y);
        }
        public static void MoveClick(int x, int y)
        {
            MouseMove(x, y);
            MouseClick(1);
        }

        public static int getCursorX()
        {
            return Cursor.Position.X;
        }

        public static int getCursorY()
        {
            return Cursor.Position.Y;
        }
    }
}