using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Reflection;
using System.Text;
using System.Threading;
using System.Collections.Generic;

namespace RemoteController
{
    public partial class HTTPServer
    {
        private Profile profile;
        private bool listening = true;
        private TcpListener listener;
        private List<string> senders = new List<string>();
        private List<string> loggedInSenders = new List<string>();

        private int Port;
        private bool serverIsPublic;

        public HTTPServer(Profile p, int portOverride)
        {
            Console.Clear();
            this.profile = p;
            initialise(portOverride);
            listener = new TcpListener(IPAddress.Any, Port);
            Thread serverThread = new Thread(new ThreadStart(Run));
            serverThread.Start();
        }

        private void initialise(int portOverride)
        {
            string hostName = Dns.GetHostName();
            IPAddress[] IPs = Dns.GetHostEntry(hostName).AddressList;
            string IP = "";
            foreach (IPAddress address in IPs)
            {
                if (address.AddressFamily == AddressFamily.InterNetwork)
                    IP = address.ToString();
            }

            g = System.Drawing.Graphics.FromImage(bmp);

            UTF8Encoding uft8 = new UTF8Encoding();
            Assembly assembly = Assembly.GetExecutingAssembly();
            StreamReader reader = new StreamReader(assembly.GetManifestResourceStream("RemoteController.Resources.pass.html"));
            passHTML = reader.ReadToEnd();
            reader = new StreamReader(assembly.GetManifestResourceStream("RemoteController.Resources.index.html"));
            indexHTML = reader.ReadToEnd();
            genToken();

            if (portOverride == -1)
                Port = profile.port;
            else
                Port = portOverride;
            serverIsPublic = profile.serverIsPublic;
            string access;
            if (serverIsPublic)
                access = "everywhere";
            else
                access = "network";

            string portOut = "";
            if (Port != 80 && Port != 8080)
                portOut = ":" + Port;

            Console.WriteLine("\nServer accessible from " + access);
            Console.WriteLine("Server is starting to listen on port " + Port);
            Console.WriteLine("\nThe Website is accessable throught the ip: " + IP + portOut + "\n");
        }

        private void Run()
        {
            listener.Start();

            while (listening)
            {
                TcpClient client = listener.AcceptTcpClient();

                HandleRequest(client);

                client.Close();
            }
        }

        private void HandleRequest(TcpClient client)
        {
            string sender = ((IPEndPoint)client.Client.RemoteEndPoint).Address.ToString();

            if (!(sender.StartsWith("192.168.") || sender.Equals("127.0.0.1") || serverIsPublic))
                return;

            NetworkStream nwStream = client.GetStream();

            byte[] buffer = new byte[client.ReceiveBufferSize];

            nwStream.Read(buffer, 0, buffer.Length);

            string[] request = Encoding.UTF8.GetString(buffer, 0, buffer.Length).Split('\n');

            sendResponse(request, nwStream, sender);
        }

        private void genToken()
        {
            string chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
            Random random = new Random();

            for (int i = 0; i < 16; i++)
                currentAccessToken += chars[random.Next(chars.Length)];
        }
    }
}