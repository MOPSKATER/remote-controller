using System.Windows.Forms;

namespace RemoteController
{
    public partial class DeviceHandler
    {
        public static void SendString(string key)
        {
            try
            {
                SendKeys.SendWait(key);
            }
            catch { }
        }
    }
}