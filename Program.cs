﻿
using System;
using System.IO;
using System.Net;
using System.Net.NetworkInformation;
using System.Reflection;
using System.Runtime.InteropServices;

namespace RemoteController
{
    class Program
    {
        [DllImport("User32.dll", CallingConvention = CallingConvention.StdCall, SetLastError = true)]
        [return: MarshalAs(UnmanagedType.Bool)]
        private static extern bool ShowWindow([In] IntPtr hWnd, [In] int nCmdShow);

        private static string programPath = Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location);
        private static void Main(string[] args)
        {
            Console.Title = "Remote Controller";


            AppDomain.CurrentDomain.AssemblyResolve += new ResolveEventHandler(loadDLL);

            bool startServer = false;
            if ((args.Length > 0 && (args[0].Equals("skip")) || locatedInAutostart()))
            {
                startServer = true;
                IntPtr handle = System.Diagnostics.Process.GetCurrentProcess().MainWindowHandle;

                ShowWindow(handle, 6);
            }

            Profile p = Profile.GetProfile();

            int portOverride = -1;

            Console.WriteLine("This program was written by MOPSKATER\nType \"help\" for help\n");

            while (!startServer)
            {
                Console.Write(">");
                string input = Console.ReadLine();
                if (input.StartsWith("help"))
                {
                    Console.WriteLine("- autostart (Will set or remove a link to the autostart)");
                    Console.WriteLine("- setpassword <new password> (Changes your password)");
                    Console.WriteLine("- setport <new port> (Changes your port (default: 80))");
                    Console.WriteLine("- togglepublic (Toggles if your server is reachable from everywhere or network only (default: false))");
                    Console.WriteLine("- reset (Deletes all configuration data)");
                    Console.WriteLine("- start (<port>) (Starts the server with an optional port override)");
                }
                else if (input.StartsWith("setpassword "))
                {
                    p.password = input.Split(' ')[1];
                    p.saveProfile();
                    Console.WriteLine("Your password was set to " + p.password);
                }
                else if (input.StartsWith("setport "))
                {
                    int port;
                    if (int.TryParse(input.Split(' ')[1], out port))
                    {
                        if (port > 0 && port < 65536)
                        {
                            p.port = port;
                            p.saveProfile();
                            Console.WriteLine("Your port was set to " + port);
                        }
                        else
                            Console.WriteLine("Ungültiges Argument (Select port from 1 to 65535)");
                    }
                    else
                        Console.WriteLine("Ungültiges Argument (Select port from 1 to 65535)");
                }
                else if (input.StartsWith("togglepublic"))
                {
                    if (p.serverIsPublic)
                    {
                        p.serverIsPublic = false;
                        Console.WriteLine("Server is now only accessable from network");
                    }
                    else
                    {
                        p.serverIsPublic = true;
                        Console.WriteLine("Server is now accessable from everywhere");
                    }
                    p.saveProfile();
                }
                else if (input.StartsWith("start"))
                {
                    if (input.Equals("start"))
                        startServer = true;
                    else if (input.ToCharArray()[5].Equals(' '))
                    {
                        int port;
                        if (int.TryParse(input.Split(' ')[1], out port))
                        {
                            if (port > 0 || port < 65536)
                                portOverride = port;

                            startServer = true;
                        }
                        else
                            Console.WriteLine("Ungültiges Argument (Select port from 1 to 65535)");
                    }
                    else
                        Console.WriteLine("Ungültiges Argument (Select port from 1 to 65535)");
                }
                else if (input.StartsWith("reset"))
                {
                    p = Profile.GetNewProfile();
                    Console.WriteLine("Configuration was reset");
                }
                else if (input.StartsWith("autostart"))
                {
                    string autostartPath = Environment.GetEnvironmentVariable("appdata") + @"\Microsoft\Windows\Start Menu\Programs\Startup";
                    if (File.Exists(autostartPath + "\\Remote Controller.exe"))
                    {
                        File.Delete(autostartPath + "\\Remote Controller.exe");
                        Console.WriteLine("This program was removed from the autostart");
                    }
                    else
                    {
                        File.Copy(programPath + "\\" + System.AppDomain.CurrentDomain.FriendlyName, autostartPath + "\\Remote Controller.exe");
                        Console.WriteLine("This program was added to the autostart");
                    }
                }
                else
                    Console.WriteLine("Unknown command. Type \"help\" for help");

                if (startServer)
                {
                    if (portOverride == -1)
                    {
                        if (!PortAvailable(p.port))
                        {
                            Console.WriteLine("Port " + p.port + " ist nicht verfügbar");
                            startServer = false;
                        }
                    }
                    else
                    {
                        if (!PortAvailable(portOverride))
                        {
                            Console.WriteLine("Port " + portOverride + " ist nicht verfügbar");
                            startServer = false;
                        }
                    }
                }
            }

            new HTTPServer(p, portOverride);
        }

        private static bool locatedInAutostart()
        {
            string[] path = programPath.Split('\\');
            if (path[path.Length - 1] == "Startup")
                return true;
            return false;
        }

        public static Assembly loadDLL(object sender, ResolveEventArgs args)
        {
            Stream stream = Assembly.GetExecutingAssembly().GetManifestResourceStream("RemoteController.Resources.Newtonsoft.Json.dll");
            byte[] buffer = new byte[stream.Length];
            stream.Read(buffer, 0, buffer.Length);
            return Assembly.Load(buffer);
        }

        public static bool PortAvailable(int port)
        {
            IPGlobalProperties ipProperties = IPGlobalProperties.GetIPGlobalProperties();
            IPEndPoint[] ipEndPoints = ipProperties.GetActiveTcpListeners();

            foreach (IPEndPoint endPoint in ipEndPoints)
                if (endPoint.Port == port)
                    return false;
            return true;
        }
    }
}