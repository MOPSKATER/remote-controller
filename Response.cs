using System;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Net.Sockets;
using System.Text;
using System.Windows.Forms;
using Newtonsoft.Json;

namespace RemoteController
{
    public partial class HTTPServer
    {
        private string passHTML;
        private string indexHTML;
        private const string header = "HTTP/1.1 200 OK\n\n";
        private const string headerWrongPass = "HTTP/1.1 401 Unauthorized\n\n";
        private const string headerSomethingWrong = "HTTP/1.1 406 Not Acceptable\n\n";
        private const string headerImage = "HTTP/1.1 200 OK\r\nContent-Type: text\r\n\r\n";
        private string currentAccessToken = "";


        Bitmap bmp = new Bitmap(200, 200);
        Graphics g;
        private ImageConverter converter = new ImageConverter();
        MemoryStream stream;
        public void sendResponse(string[] request, NetworkStream nwStream, string sender)
        {
            string cookie = getCookie(request);
            string[] headlineHeadder = request[0].Split(' ');
            string type = headlineHeadder[0];

            if (type.Equals("GET"))
            {
                if (cookie.Equals(currentAccessToken) || profile.password == "")
                {
                    if (headlineHeadder[1] == "/image")
                    {
                        try
                        {
                            g.CopyFromScreen(Cursor.Position.X - 100, Cursor.Position.Y - 100, 0, 0, bmp.Size);
                        }
                        catch { }
                        stream = new MemoryStream();
                        bmp.Save(stream, ImageFormat.Gif);
                        byte[] img = stream.ToArray();
                        string base64img = Convert.ToBase64String(img);
                        POST(headerImage + base64img, nwStream);
                    }
                    else
                        POST(header + indexHTML, nwStream);
                }
                else
                {
                    Console.WriteLine(sender + " received the login screen");
                    POST(header + passHTML, nwStream);
                }
            }
            else if (type.Equals("POST"))
            {
                if (cookie.Equals(currentAccessToken) || profile.password == "")
                {
                    bool? save = null;
                    string line = null;
                    foreach (string s in request)
                    {
                        if (s.StartsWith("EXEC"))
                        {
                            save = null;
                            line = s.Substring(4, s.Length - 5);
                            break;
                        }
                        else if (s.StartsWith("SAVE"))
                        {
                            save = true;
                            line = s.Substring(5, s.Length - 6);
                            break;
                        }
                        else if (s.StartsWith("DEL"))
                        {
                            save = false;
                            line = s.Substring(3, s.Length - 4);
                            break;
                        }
                        else if (s.StartsWith("MOUSE"))
                        {
                            doMouseThings(s.Substring(6, s.Length - 7), nwStream);
                            break;
                        }
                    }

                    if (line != null)
                    {
                        if (save == true)
                        {
                            if (line.StartsWith("K"))
                            {
                                try
                                {
                                    profile.addEntry(line);
                                    string[] parts = line.Split('⠀');
                                    string poststring = header + parts[0] + "⠀" + parts[1];
                                    if (parts.Length == 4)
                                        poststring += "⠀" + parts[3];
                                    POST(header + parts[0] + "⠀" + parts[1], nwStream);
                                }
                                catch
                                {
                                    Console.WriteLine("Parsing error: " + line);
                                    POST(headerSomethingWrong, nwStream);
                                }
                            }
                            else
                            {
                                string[] parts = line.Split('⠀');
                                string postString = header + line;
                                string saveString = line[0] + line[1] + "⠀" + DeviceHandler.getCursorX() + ";" + DeviceHandler.getCursorY() + "⠀" + line[2];
                                if (parts.Length == 3)
                                    postString += "⠀" + parts[3];
                                profile.addEntry(saveString);
                                POST(postString, nwStream);
                            }
                        }
                        else if (save == false)
                        {
                            int idx;
                            if (int.TryParse(line, out idx))
                            {
                                profile.removeEntry(idx);
                                POST(nwStream);
                            }
                            else
                            {
                                Console.WriteLine("Parsing error: " + line);
                                POST(headerSomethingWrong, nwStream);
                            }
                        }
                        else
                        {
                            int idx;
                            if (int.TryParse(line, out idx))
                            {
                                string shortcut = profile.getShortcutEntry(idx);
                                if (shortcut != null)
                                {
                                    string[] instructions = shortcut.Split('⠀');
                                    if (instructions[0] == "K")
                                    {
                                        DeviceHandler.SendString(instructions[2]);
                                        POST(nwStream);
                                    }
                                    else
                                    {
                                        int x = int.Parse(instructions[2].Split(';')[0]);
                                        int y = int.Parse(instructions[2].Split(';')[1]);
                                        DeviceHandler.MoveClick(x, y);
                                        POST(nwStream);
                                    }
                                }
                                else
                                {
                                    Console.WriteLine("Failed to get entry " + idx);
                                    POST(headerSomethingWrong, nwStream);
                                }
                            }
                            else
                            {
                                Console.WriteLine("Parsing error: " + line);
                                POST(headerSomethingWrong, nwStream);
                            }
                        }
                    }
                }
                else
                {
                    string pass = "";
                    for (int i = 0; i < request.Length; i++)
                        if (request[i].Contains("password"))
                            pass = request[i + 2].Replace("\r", "");

                    if (pass == profile.password)
                    {
                        Console.WriteLine(sender + " logged successfully in");
                        POST(header + currentAccessToken, nwStream);
                    }
                    else
                    {
                        Console.WriteLine(sender + " failed to login");
                        POST(headerWrongPass, nwStream);
                    }
                }
            }
            else
            {
                string[] shortcuts = profile.getShortcuts();
                POST(header + JsonConvert.SerializeObject(shortcuts), nwStream);
            }
        }

        private void doMouseThings(string command, NetworkStream nwStream)
        {
            string[] subcommands = command.Split('⠀');
            if (subcommands[0] == "M")
            {
                bool succeed = true;
                string[] mouseParams = subcommands[1].Split(';');
                int lastX = 0, lastY = 0, x = 0, y = 0;
                succeed = int.TryParse(mouseParams[0], out lastX) && int.TryParse(mouseParams[1], out lastY) && int.TryParse(mouseParams[2], out x) && int.TryParse(mouseParams[3], out y);
                if (succeed)
                {
                    double deltaX = (x - lastX) * 0.2;
                    double deltaY = (y - lastY) * 0.2;
                    double dist = Math.Sqrt(deltaX * deltaX + deltaY * deltaY);
                    DeviceHandler.MouseMove((int)(Cursor.Position.X + deltaX * dist / 20), (int)(Cursor.Position.Y + deltaY * dist / 20));
                    POST(nwStream);
                }
                else
                    POST(headerSomethingWrong, nwStream);
            }
            else
            {
                int clicks = 0;
                if (int.TryParse(subcommands[1], out clicks))
                    DeviceHandler.MouseClick(clicks);
                POST(nwStream);
            }
        }

        private string getCookie(string[] request)
        {
            foreach (string line in request)
            {
                if (line.StartsWith("Cookie: "))
                {
                    string output = line.Replace("Cookie: ", "") + "; miau";
                    foreach (string cookiePair in output.Split("; ".ToCharArray()))
                        if (cookiePair.StartsWith("session="))
                            return cookiePair.Substring(8, 16);
                }
            }

            return "";
        }

        private void POST(NetworkStream nwStream)
        {
            UTF8Encoding uft8 = new UTF8Encoding();
            byte[] send = uft8.GetBytes(header);
            nwStream.Write(send, 0, send.Length);
        }

        private void POST(String msg, NetworkStream nwStream)
        {
            UTF8Encoding uft8 = new UTF8Encoding();
            byte[] send = uft8.GetBytes(msg);
            nwStream.Write(send, 0, send.Length);
        }

        private void POST(byte[] msg, NetworkStream nwStream)
        {
            nwStream.Write(msg, 0, msg.Length);
        }
    }
}