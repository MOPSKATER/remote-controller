using Newtonsoft.Json;

using System;
using System.IO;
using System.Collections.Generic;

namespace RemoteController
{
    public class Profile
    {

        public string password { get; set; } = "Cats4WIN!";
        public int port { get; set; } = 80;
        public bool serverIsPublic { get; set; } = false;

        public Dictionary<int, string> shortcuts = new Dictionary<int, string>();

        private static readonly string filePath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments) + "\\RemoteController\\";
        private static readonly string fileName = "profile.json";

        public static Profile GetProfile()
        {
            Profile p = new Profile();
            if (File.Exists(filePath + fileName))
            {
                try
                {
                    return JsonConvert.DeserializeObject<Profile>(File.ReadAllText(filePath + fileName));
                }
                catch
                {
                    Console.WriteLine("Profile.json is corrupted. Creating new profile.json and a backup of the old one");
                    File.Copy(filePath + fileName, filePath + fileName + " old");
                    Directory.CreateDirectory(filePath);
                    File.Create(filePath + fileName).Close();
                }
            }
            else
            {
                Directory.CreateDirectory(filePath);
                File.Create(filePath + fileName).Close();
            }
            Console.WriteLine("The default password is Cats4WIN!");
            p.saveProfile();
            return p;
        }

        public static Profile GetNewProfile()
        {
            Profile p = new Profile();
            p.saveProfile();
            return p;
        }

        public void saveProfile()
        {
            File.WriteAllText(filePath + fileName, JsonConvert.SerializeObject(this));
        }

        public void addEntry(string shortcut)
        {
            shortcuts.Add(shortcuts.Count, shortcut);
            saveProfile();
        }

        public void removeEntry(int idx)
        {
            shortcuts.Remove(idx);
            saveProfile();
        }

        public string getShortcutEntry(int idx)
        {
            string ret = null;
            shortcuts.TryGetValue(idx, out ret);
            return ret;
        }

        public string[] getShortcuts()
        {
            string[] ret = new string[shortcuts.Keys.Count];
            for (int i = 0; i < ret.Length; i++)
            {
                string val;
                shortcuts.TryGetValue(i, out val);
                string[] parts = val.Split('⠀');
                ret[i] = parts[0] + "⠀" + parts[1];
                if (parts.Length == 4)
                    ret[i] += "⠀" + parts[3];
            }
            return ret;
        }
    }
}